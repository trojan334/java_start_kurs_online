package com.company.library.model;


public class Library {
    private static final int MAX_PUBLIATIONS = 2000;
    private Publication[] publications;
    private int publicationsNumber;


    public Publication[] getPublications() {
        return publications;
    }

    public int getPublicationsNumber() {
        return publicationsNumber;
    }


    public Library() {
        publications = new Publication[MAX_PUBLIATIONS];
    }

    public void addBook(Book book) {
        addPublication(book);
    }

    public void addMagazine(Magazine magazine) {
        addPublication(magazine);
    }


    private void addPublication(Publication pub) {
        if (publicationsNumber == MAX_PUBLIATIONS) {
            throw new ArrayIndexOutOfBoundsException("MAX_PUBLICATIONS" + MAX_PUBLIATIONS);
        }
        publications[publicationsNumber] = pub;
        publicationsNumber++;
    }

}

