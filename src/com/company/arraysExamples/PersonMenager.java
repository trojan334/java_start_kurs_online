package com.company.arraysExamples;

public class PersonMenager {
    public static void main(String[] args) {
        PersonDatabase pdb = new PersonDatabase();
        pdb.add(new Person("Ania","Malinka", "131531"));
        pdb.add(new Person("Karol","Ogórek", "554665"));
        pdb.add(new Person("Basia","Kalafiorek", "135151"));

        System.out.println(pdb);
        pdb.remove(new Person("Ania","Malinka", "131531"));
        System.out.println();
        System.out.println(pdb);
    }
}
