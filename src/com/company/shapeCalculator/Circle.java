package com.company.shapeCalculator;

public class Circle implements Shape {
    private double r;

    public Circle(double r) {
        this.r = r;
    }

    public double getR() {
        return r;
    }

    public void setR(double r) {
        this.r = r;
    }

    @Override
    public double calculateArea() {
        return Shape.PI*r*r;
    }

    @Override
    public double calculatePerimeter() {
        return 2*Shape.PI*r;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("Koło, ");
        sb.append("promień: ").append(r).append(", ");
        sb.append("pole: ").append(calculateArea()).append(", ");
        sb.append("obwód: ").append(calculatePerimeter());
        return sb.toString();
    }
}
