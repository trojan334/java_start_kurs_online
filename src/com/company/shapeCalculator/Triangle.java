package com.company.shapeCalculator;

public class Triangle implements Shape {
    private double height;
    private double a;
    private double b;
    private double c;

    public Triangle(double height, double a, double b, double c) {
        this.height = height;
        this.a = a;
        this.b = b;
        this.c = c;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public double getA() {
        return a;
    }

    public void setA(double a) {
        this.a = a;
    }

    public double getB() {
        return b;
    }

    public void setB(double b) {
        this.b = b;
    }

    public double getC() {
        return c;
    }

    public void setC(double c) {
        this.c = c;
    }

    @Override
    public double calculateArea() {
        return a*height/2;
    }

    @Override
    public double calculatePerimeter() {
        return a+b+c;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("Trójkąt, ");
        sb.append("bok A: ").append(a).append(", ");
        sb.append("bok B:").append(b).append(", ");
        sb.append("bok c:").append(c).append(", ");
        sb.append("wysokość").append(height).append(", ");
        sb.append("pole: ").append(calculateArea()).append(", ");
        sb.append("obwód: ").append(calculatePerimeter());
        return sb.toString();
    }
}
