package com.company;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class ListExamples {
    public static final String EXIT = "exit";
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        List<Double> array = new ArrayList<>();
        fillList(array);
        printData(array);
    }


    static void fillList(List<Double> list) {
        System.out.println("Podaj kolejną liczbę (lub wpisz \"exit\"): ");

        String input = scanner.nextLine();
        if (input.equals(ListExamples.EXIT)) {
            return;
        }

        try {
            Double num = Double.valueOf(input);
            list.add(num);
        } catch (NumberFormatException e) {
            System.err.println("Liczba w niepoprawnym formacie: ");
        }

        fillList(list);
    }

    static void printData(List<Double> list) {
        StringBuilder stringBuilder = new StringBuilder();

        double sum = 0;

        for (Double aDouble : list) {
            stringBuilder.append(aDouble);
            stringBuilder.append('+');
            sum += aDouble;
        }

        stringBuilder.replace(stringBuilder.length() - 1, stringBuilder.length(), "=");
        stringBuilder.append(sum);
        System.out.println(stringBuilder.toString());
    }
}
