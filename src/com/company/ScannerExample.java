package com.company;

import java.util.Scanner;

public class ScannerExample {
    public static void main(String[] args) {
        Scanner reader = new Scanner(System.in);

        int userInput;
        int counter = 0;

        System.out.println("Zacznij wprowadzać liczby, wprowadzanie zostanie zakończone gdy podasz liczbę więksą od 100.");

        while ((userInput = reader.nextInt()) <=100) {
           counter+=userInput;
            System.out.println("Podaj kolejną liczbę: ");
        }
        System.out.println(counter%2 == 0 ? "parzysta" : "nieparzysta");
        reader.close();
    }
}
