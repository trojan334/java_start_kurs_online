package com.company;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MethodRefExercise {
    public static void main(String[] args) {
        List<String> names = new ArrayList<>();
        names.add("Łukasz");
        names.add("Kasia");
        names.add("Natalia");
        names.add("Wojciech");
        names.add("Halina");

        names.sort(String::compareToIgnoreCase);
        names.forEach(System.out::println);

    }

}
