package com.company;

import java.io.*;
import java.math.BigInteger;

public class ReadNumbers {
    public static void main(String[] args) {
        int a = 0;
        int b = 0;
        int c = 0;
        BigInteger aBig = null;
        BigInteger bBig = null;
        String fileName = "numbers.txt";

        try (FileReader fileReader = new FileReader(fileName);
             BufferedReader bufferedReader = new BufferedReader(fileReader)){
            a=Integer.valueOf(bufferedReader.readLine());
            b=Integer.valueOf(bufferedReader.readLine());
            c=Integer.valueOf(bufferedReader.readLine());
            aBig = new BigInteger(bufferedReader.readLine());
            bBig = new BigInteger(bufferedReader.readLine());
            System.out.println("a+b+c =" + (a+b+c));
            System.out.println("aBig + bBig = " + (aBig.add(bBig)));
        }catch (FileNotFoundException e){
            e.printStackTrace();
            System.out.println("Nie znaleziono pliku");
        } catch (IOException e){
            e.printStackTrace();
            System.out.println("Błąd odczytu pliku");
        }
    }
}
