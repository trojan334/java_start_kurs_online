package com.company.school;

public class University {
    public static void main(String[] args) {
        System.out.println("Liczba studentów przed zapisami: " + Student.getStudentsNumber());
        Student s1 = new Student("Łukasz", "Trojanowicz", 175856);
        Student s2 = new Student("Jan", "Kowalski", 556352);
        System.out.println("Liczba studentów po zapisach: " + Student.getStudentsNumber());
    }
}
