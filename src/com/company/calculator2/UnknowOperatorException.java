package com.company.calculator2;

public class UnknowOperatorException extends RuntimeException{
    public UnknowOperatorException(String message){
        super(message);
    }
}
