package com.company.calculator2;

public class Calculator {
    public static final String PLUS = "+";
    public static final String MINUS = "-";
    public static final String MULTIPLY = "*";
    public static final String DIVIDE = "/";

    public double caclulate(double a, double b, String operator) {
        double result = 0;

        switch (operator) {
            case PLUS:
                result = a + b;
                break;
            case MINUS:
                result = a - b;
                break;
            case MULTIPLY:
                result = a * b;
                break;
            case DIVIDE:
                if (b == 0) {
                    throw new ArithmeticException("Nie mozesz dzielić przez 0!");
                }
                result = a / b;
                break;
            default:
                throw new UnknowOperatorException("Podałeś zły operator");
        }
        return result;
    }
}
