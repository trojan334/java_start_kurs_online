package com.company.automotiveWholesaler;

public class Part {
    public int id;
    public String manufacturer;
    public String productName;
    public String productSeries;
}