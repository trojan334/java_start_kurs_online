package com.company;

public class ArraysExamples {
    public static void main(String[] args) {
        int[] tab1 = {1, 5, 8};
        int[] tab2 = {3, 5, 8};
        System.out.println(sumOfElements(tab1, tab2));
    }

    static int sumOfElements(int[] tab1, int[] tab2) {
        int sum = 0;
        for (int i = 0; i < tab1.length; i++) {
            sum += tab1[i] + tab2[i];
        }
        return sum;
    }
}
