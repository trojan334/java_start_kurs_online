package com.company;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class StreamExerciseFile {
    private static final String FILE_PATH = "loremipsum-129.txt";

    public static void main(String[] args) {
        List<String> word = Arrays.asList(read(FILE_PATH).split(" "));

        List<String> collect = word.stream()
                .map(x -> x.replace(",", ""))
                .map(x -> x.replace(".", ""))
                .collect(Collectors.toList());

        int count = (int) word.stream()
                .filter(x -> x.startsWith("s"))
                .count();
        System.out.println("liczba słów zaczynających się od s: " + count);

        long count1 = word.stream()
                .filter(s -> s.length() == 5)
                .count();
        System.out.println("Liczba słów 5 literowych " + count1);
    }

    private static String read(String file) {
        StringBuilder stringBuilder = new StringBuilder();
        try (Scanner fileScaner = new Scanner(new File(file))) {
            while (fileScaner.hasNextLine()) {
                String line = fileScaner.nextLine();
                stringBuilder.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return stringBuilder.toString();
    }
}
