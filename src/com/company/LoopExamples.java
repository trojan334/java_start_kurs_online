package com.company;

import java.util.Scanner;

public class LoopExamples {
    public static void main(String[] args) {
        Scanner scanner =new Scanner(System.in);

        int temp;

        for (int i = 0; i < 3; i++) {
            System.out.println("Podaj liczbę: ");
            temp = scanner.nextInt();

            System.out.printf("Liczba: %s jest %s",temp,temp%2==0 ? "parzysta\n" : "nieparzysta\n");

        }
        scanner.close();
    }

}
