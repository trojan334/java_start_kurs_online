package com.company.banckAccountExercise;

public class Bank {
    public static void main(String[] args) {

        // first Person
        Person person1 = new Person();
        person1.firstName="Jan";
        person1.lastName="Kowalski";
        person1.pesel="9012112362";

        //address
        Address address1 = new Address();
        address1.city="Warszawa";
        address1.postalCode="20-222";
        address1.street="Woronicza";
        address1.houseNumber="22";
        address1.apartmentNumber = "43";

        // address living and registered are the same
        person1.livingAdress=address1;
        person1.registeredAdress=address1;

        // first bank account
        BankAccount account1= new BankAccount();
        account1.owner=person1;
        account1.balance=10_000;

        // second person
        Person person2 = new Person();
        person2.firstName = "Marta";
        person2.lastName="Kowalska";
        person2.pesel="91070645628";
        person2.registeredAdress = new Address();
        person2.registeredAdress.city="Kraków";
        person2.registeredAdress.postalCode="30-333";
        person2.registeredAdress.street="Mickiewicza;";
        person2.registeredAdress.houseNumber="15";
        person2.registeredAdress.apartmentNumber="8";
        person2.livingAdress = address1;

        //second bank account
        BankAccount account2 = new BankAccount();
        account2.owner=person2;
        account2.balance=5_000;

        System.out.println("Osoba 1:");
        System.out.println(person1.firstName + " " + person1.lastName);
        System.out.println("mieszka w miejscowości: " + person1.livingAdress.city);
        System.out.println("posiada konto bankowe z kwotą: " + account1.balance);

        System.out.println("Osoba 1:");
        System.out.println(person2.firstName + " " + person2.lastName);
        System.out.println("mieszka w miejscowości: " + person2.livingAdress.city);
        System.out.println("posiada konto bankowe z kwotą: " + account2.balance);
    }
}
