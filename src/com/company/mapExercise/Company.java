package com.company.mapExercise;

import java.util.HashMap;

public class Company {

    private HashMap<String, Employee> employeys = new HashMap<>();

    public boolean addEmployee(Employee employee) {
        String key = employee.getFirstName() + " " + employee.getLastName();

        if (employeys.get(key) != null) {
            return false;
        } else {
            employeys.put(key, employee);
            return true;
        }
    }

    public Employee getEmployee(String firstName, String lastName) {
        String key = firstName + " " + lastName;
        return employeys.get(key);
    }
}
