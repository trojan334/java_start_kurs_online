package com.company.mapExercise;

import java.util.Scanner;

public class CompanyApp {
    public static final int ADD_EMPLOYEE = 0;
    public static final int SEARCH_EMPLOYEE = 1;
    public static final int EXIT = 2;

    private static Company company = new Company();

    public static void main(String[] args) {
        try (Scanner scanner = new Scanner(System.in)) {
            int userOption;
            do {
                printOptions();
                userOption = scanner.nextInt();
                scanner.nextLine();

                switch (userOption) {
                    case CompanyApp.ADD_EMPLOYEE:
                        addEmployee(scanner);
                        break;
                    case CompanyApp.SEARCH_EMPLOYEE:
                        findAndPrintEmployee(scanner);
                    case CompanyApp.EXIT:
                        break;
                }
            } while (userOption != CompanyApp.EXIT);
        }
    }

    private static void findAndPrintEmployee(Scanner scanner) {
        System.out.println("Wyszukiwanie pracownika: ");
        System.out.println("Podaj imię: ");
        String firstName = scanner.nextLine();
        System.out.println("Podaj nazwisko: ");
        String lastName = scanner.nextLine();

        Employee employee = company.getEmployee(firstName, lastName);
        System.out.println(employee);
    }

    private static void addEmployee(Scanner scanner) {
        Employee employee = new Employee();

        System.out.println("Dodawanie pracownika.");
        System.out.println("Imię: ");
        employee.setFirstName(scanner.nextLine());
        System.out.println("Nazwisko: ");
        employee.setLastName(scanner.nextLine());
        System.out.println("Wynagordznie: ");
        employee.setSalary(scanner.nextDouble());
        scanner.nextLine();

        company.addEmployee(employee);
    }

    private static void printOptions() {
        System.out.println("0 - dodanie pracownika");
        System.out.println("1 - wyszukiwanie pracownika");
        System.out.println("2 - wyjście");
    }


}
