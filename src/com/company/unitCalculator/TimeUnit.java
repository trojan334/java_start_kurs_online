package com.company.unitCalculator;

public class TimeUnit {

    static int hoursPerMinutes(int hours) {
        return hours * 60;
    }

    static int minutesPerSeconds(int minutes) {
        return minutes * 60;
    }

    static int secondsPerMiliseconds(int seconds) {
        return seconds * 1000;
    }
}
