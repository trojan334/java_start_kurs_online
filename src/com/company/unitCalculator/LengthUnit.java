package com.company.unitCalculator;

public class LengthUnit {

    static double metersPerCentimeters(double meters) {
        return meters * 100;
    }

    static double metersPerMilimeters(double meters) {
        return meters * 1000;
    }

    static double centimetersPerMeters(double cenitmeters) {
        return cenitmeters * 0.01;
    }

    static double milimetersPerMeters(double milimeters) {
        return milimeters * 0.001;
    }
}
