package com.company;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StreamExample {
    public static void main(String[] args) {
        List<Integer> numbers = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
        numbers = numbers.stream()
                .filter(x -> x % 2 == 0)
                .filter(x -> x > 5)
                .collect(Collectors.toList());

        numbers.forEach(System.out::println);

        Stream<String> strings = Stream.of("a", "bb", "ccc", "dddd", "eeeee", "ffffff", "ggggggg");
        List<String> stringList = strings
                .peek(System.out::println)
                .map(String::toUpperCase)
                .peek(System.out::println)
                .collect(Collectors.toList());

        Stream<Integer> numStream = Stream.iterate(0, x -> x + 1);

        //lista 100 pierwszych liczb podzielnych przez 2
        List<Integer> numbers2 = numStream.filter(x -> x % 2 == 0).limit(100).collect(Collectors.toList());

        //lista kwadratów kolejnych 100 liczb całkowitych
        List<Integer> squareNumbers = numStream.map(x->x*x).limit(100).collect(Collectors.toList());

        //lista 100 liczb ujemnych w kolejności malejącej
        List<Integer> negativeNumbers = numStream.map(x->-x).limit(100).collect(Collectors.toList());
    }
}

