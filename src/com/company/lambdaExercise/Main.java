package com.company.lambdaExercise;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.function.Consumer;
import java.util.function.IntSupplier;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Main {
    public static void main(String[] args) {
        Random random = new Random();
        List<Integer> ints = new ArrayList<>();
        //generate numbers
        generate(ints, 10, () -> random.nextInt(1000));
        //printing
        consume(ints, x -> System.out.print(x + " "));
        System.out.println();
        //deleting division by 2
        List<Integer> filteredList = ints.stream().filter(x -> x % 2 == 1).collect(Collectors.toList());
        //pringing
        consume(filteredList, x -> System.out.print(x + " "));


    }

    private static <T> void consume(List<T> list, Consumer<T> consumer) {
        for (T t : list) {
            consumer.accept(t);
        }
    }

    private static <T> void generate(List<T> list, int toGenerate, Supplier<T> sup) {
        for (int i = 0; i < toGenerate; i++) {
            list.add(sup.get());
        }
    }
}
