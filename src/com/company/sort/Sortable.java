package com.company.sort;

public interface Sortable {
    int[]sort(int[] tab);
}
