package com.company.carShop.data;

public class Car {
    //Fields
    public int doors;
    public String carColor;
    public String wheelsColor;
    public String tiresColor;
    public String brand;
    public String model;

    //Constructors
    public Car() {

    }

    //Setters & getters
    public int getDoors() {
        return doors;
    }

    public void setDoors(int doors) {
        this.doors = doors;
    }

    public String getCarColor() {
        return carColor;
    }

    public void setCarColor(String carColor) {
        this.carColor = carColor;
    }

    public String getWheelsColor() {
        return wheelsColor;
    }

    public void setWheelsColor(String wheelsColor) {
        this.wheelsColor = wheelsColor;
    }

    public String getTiresColor() {
        return tiresColor;
    }

    public void setTiresColor(String tiresColor) {
        this.tiresColor = tiresColor;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }
}
