package com.company.carShop.logic;

import com.company.carShop.data.Car;

public class CarShop {
    public static void main(String[] args) {
        Car audi = new Car();
        audi.setBrand("Audi");
        audi.setModel("A4");
        audi.setDoors(4);
        audi.setCarColor("Czarny");
        audi.setWheelsColor("Srebrny");
        audi.setTiresColor("Czarny");

        String audiInfo = audi.getBrand() +" "+ audi.getModel()
                +", Drzwi: "+audi.getDoors()
                +", Kolor nadwozia: " + audi.getCarColor()
                +", Kolor felg: " + audi.getWheelsColor()
                +", Kolor opon: " +audi.getTiresColor();
        System.out.println("Wybrałeś następujący samochód: ");
        System.out.println(audiInfo);
    }
}
