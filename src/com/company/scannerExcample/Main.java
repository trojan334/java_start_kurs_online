package com.company.scannerExcample;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Calculator calculator = new Calculator();

        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj pierwszą liczbę: ");
        double a = scanner.nextDouble();
        System.out.println("Podaj działanie w postaci znaku (+, -, /, *): ");
        String action = scanner.next();
        System.out.println("Podaj drugą liczbę: ");
        double b = scanner.nextDouble();

        switch (action) {
            case "+":
                calculator.addAndPrint(a, b);
                break;
            case "-":
                calculator.subtractAndPrint(a, b);
                break;
            case "/":
                calculator.divideAndPrint(a, b);
                break;
            case "*":
                calculator.multiplyAndPrint(a, b);
                break;
            default:
                System.out.println("Podałeś zły znak!");

        }
    }


}
