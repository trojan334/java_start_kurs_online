package com.company.scannerExcample;

public class Calculator {
    double add(double a, double b) {
        return a + b;
    }

    void addAndPrint(double a, double b) {
        double result = a + b;
        System.out.println(result);
    }

    double subtract(double a, double b) {
        return a - b;
    }

    void subtractAndPrint(double a, double b) {
        System.out.println(subtract(a, b));
    }

    double multiply(double a, double b) {
        return a * b;
    }

    void multiplyAndPrint(double a, double b) {
        System.out.println(multiply(a, b));
    }

    double divide(double a, double b) {
        return a / b;
    }

    void divideAndPrint(double a, double b) {
        System.out.println(divide(a, b));
    }
}
