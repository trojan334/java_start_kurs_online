package com.company.hospitalApp;

public class Hospital {
    private final int maxPateintsNumber = 10;
    private Patient[] patients = new Patient[maxPateintsNumber];
    private int regPatient = 0;

    public void addPatient(Patient patient) {
        if (regPatient < maxPateintsNumber) {
            patients[regPatient] = patient;
            regPatient++;
        } else {
            System.out.println("Zapisano maksymalną liczbę pacjentów, zapraszamy jutro!");
        }
    }

    public void printPatients() {
        for (int i = 0; i < regPatient; i++) {
            System.out.println(patients[i].getName() + " "
                    + patients[i].getSurname() + " "
                    + patients[i].getPesel());
        }
    }
}
