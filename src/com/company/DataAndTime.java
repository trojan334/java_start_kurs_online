package com.company;

import java.time.Duration;
import java.time.Instant;
import java.util.Scanner;

public class DataAndTime {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        System.out.println("W celu rozpoczęcia pomiaru wciśnij eneter.");
        scanner.nextLine();
        Instant i1 = Instant.now();
        System.out.println("W celu zakończenia pomiaru wciąnij enter.");
        scanner.nextLine();
        Instant i2 = Instant.now();

        Duration duration = Duration.between(i1, i2);

        System.out.println("Upłynęło " + duration.getSeconds() + " sekund.");
    }
}
