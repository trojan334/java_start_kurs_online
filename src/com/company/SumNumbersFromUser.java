package com.company;

import java.util.Scanner;

public class SumNumbersFromUser {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Podaj ile liczb chcesz wprowadzić");
        int numbersDeclaration = scanner.nextInt();
        int counter = 0;

        for (int i = 0; i < numbersDeclaration; i++) {
            System.out.println("Podaj liczbę: ");
            counter += scanner.nextInt();
        }
        System.out.println("Suma wszystkich liczb to: " + counter);
    }
}
