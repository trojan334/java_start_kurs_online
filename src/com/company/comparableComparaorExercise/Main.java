package com.company.comparableComparaorExercise;

import java.util.Arrays;
import java.util.Comparator;

public class Main {
    public static void main(String[] args) {
        Integer[] tab = {2, 5, 3, 120, 15, 64, 51, 8, 6, 57, 8, 13, 5487, 61, 65, 11, 23, 2, 20, 15};

        System.out.println("Rosnąco");

        Arrays.sort(tab);
        System.out.println(Arrays.toString(tab));

        System.out.println("Malejąco: ");
        Arrays.sort(tab, new Comparator<Integer>() {
            @Override
            public int compare(Integer o1, Integer o2) {
                return o2.compareTo(o1);
            }
        });

        System.out.println(Arrays.toString(tab));
    }
}
