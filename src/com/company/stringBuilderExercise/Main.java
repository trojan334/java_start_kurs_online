package com.company.stringBuilderExercise;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Ile wyrazów wprowadzisz?");
        int numberOfWords = scanner.nextInt();
        scanner.nextLine();

        String[] words = new String[numberOfWords];

        for (int i = 0; i < numberOfWords; i++) {
            System.out.println("Podaj kolejne słowo: ");
            words[i] = scanner.nextLine();
        }

        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < words.length; i++) {
            stringBuilder.append(words[i].charAt(words[i].length() - 1));
        }

        System.out.println(stringBuilder.toString());
    }
}
