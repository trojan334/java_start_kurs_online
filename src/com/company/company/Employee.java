package com.company.company;

public class Employee {
    private String name;
    private String surname;
    private int yearOfBirth;
    private int seniority;

    public Employee(String name, String surname, int yearOfBirth, int seniority) {
        this.name = name;
        this.surname = surname;
        this.yearOfBirth = yearOfBirth;
        this.seniority = seniority;
    }

    @Override
    public String toString() {
        return "Pracownik: " +
                "imię='" + name + '\'' +
                ", nazwisko='" + surname + '\'' +
                ", rok urodzenia=" + yearOfBirth +
                ", staż pracy=" + seniority +
                " lat.";
    }

}
