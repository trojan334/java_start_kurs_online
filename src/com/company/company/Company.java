package com.company.company;

public class Company {
    public static void main(String[] args) {
        Employee employee1 = new Employee("Łukasz","Trojanowicz",1988,10);
        Employee employee2 = new Employee("Jan","Nowak",1985,5);
        Employee employee3 = new Employee("Adam","Kowalski",1968,50);

        System.out.println(employee1.toString());
        System.out.println(employee2.toString());
        System.out.println(employee3.toString());
    }
}
