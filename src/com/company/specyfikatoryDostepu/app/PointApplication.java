package com.company.specyfikatoryDostepu.app;

import com.company.specyfikatoryDostepu.controller.PointControler;
import com.company.specyfikatoryDostepu.data.Point;

public class PointApplication {

    public static final int ADD_X = 0;
    public static final int ADD_Y = 1;
    public static final int MINUS_X = 2;
    public static final int MINUS_Y = 3;

    public static void main(String[] args) {


        Point point1 = new Point(10, 20);
        PointControler pointControler = new PointControler();
        System.out.println("Punkt przed zmiana: (" + point1.getX() + ";" + point1.getY() + ")");

        int option = ADD_X;

        switch (option) {
            case ADD_X:
                pointControler.addX(point1);
                break;
            case ADD_Y:
                pointControler.addY(point1);
                break;
            case MINUS_X:
                pointControler.minusX(point1);
                break;
            case MINUS_Y:
                pointControler.minusY(point1);
                break;
            default:
                System.out.println("Podano błędną wartość.");
        }

        System.out.println("Punkt po zmianie:("+point1.getX()+";"+point1.getY()+")");

    }
}
