package com.company.specyfikatoryDostepu.controller;

import com.company.specyfikatoryDostepu.data.Point;

public class PointControler {

    public void addX(Point point) {
        point.setX(point.getX() + 1);
    }

    public void minusX (Point point){
        point.setX(point.getX()-1);
    }

    public void addY (Point point){
        point.setY(point.getY()+1);
    }

    public void minusY (Point point){
        point.setY(point.getY()-1);
    }
}
