package com.company.GenericsExample;


public class Main {
    public static void main(String[] args) {
        Pair<Integer, Integer> pair1 = new Pair<>(10, 0);
        Pair<String, Boolean> pair2 = new Pair<>("Siemanko", true);
        Pair<String, String> pair3 = new Pair<>("Siemank ", "Lukasz");

        printPair(pair1);
        printPair(pair2);
        printPair(pair3);


    }

    static <T, V> void printPair(Pair<T, V> pair) {
        System.out.println(pair.getFirst() + " " + pair.getSecond());
    }
}
