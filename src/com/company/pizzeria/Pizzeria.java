package com.company.pizzeria;

import java.util.Scanner;

public class Pizzeria {
    public static void main(String[] args) {
        System.out.println("Dostępne pizze:");
        for (Pizza pizza : Pizza.values()) {
            System.out.println(pizza);
        }

        Scanner input = new Scanner(System.in);
        System.out.println("Jaką pizze wybierasz?");
        Pizza pizza = Pizza.valueOf(input.nextLine().toUpperCase());

        System.out.println("Dziękujemy za zamówienie pizzy " + pizza.name());
        input.close();
    }
}
