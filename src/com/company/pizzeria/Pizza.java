package com.company.pizzeria;

public enum Pizza {
    MARGHERITA(true, true, false, false),
    CAPRICIOSA(true, true, true, false),
    PROSCIUTTO(true, true, false, true);

    private boolean tomatoSouce;
    private boolean cheese;
    private boolean mushrooms;
    private boolean ham;

    //consturctors
    Pizza(boolean tomatoSouce, boolean cheese, boolean mushrooms, boolean ham) {
        this.tomatoSouce = tomatoSouce;
        this.cheese = cheese;
        this.mushrooms = mushrooms;
        this.ham = ham;
    }
    //getters

    public boolean isTomatoSouce() {
        return tomatoSouce;
    }

    public boolean isCheese() {
        return cheese;
    }

    public boolean isMushrooms() {
        return mushrooms;
    }

    public boolean isHam() {
        return ham;
    }

    @Override
    public String toString() {
        String result = this.name() + "(";
        //components informations
        if (tomatoSouce) {
            result = result + "sos pomidorowy";
        }
        if (cheese) {
            result = result + ", ser";
        }
        if (mushrooms) {
            result = result + ", pieczarki";
        }
        if (ham) {
            result = result + ", szynka";
        }
        result = result + ")";

        return result;
    }
}
