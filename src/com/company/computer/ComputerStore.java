package com.company.computer;

public class ComputerStore {
    public static void main(String[] args) {
        Computer comp1 = new Computer("Intel i5",8192);
        Computer comp2 = new Computer("AMD Ryzen 1700", 16384);

        //klient chce dodać jeszcze 4gb ramu
        ComputerUpgrade upgrade = new ComputerUpgrade();
        upgrade.addMemory(comp1,4096);

        comp1.printInfo();
        comp2.printInfo();
    }
}
