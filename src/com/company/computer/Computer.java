package com.company.computer;

public class Computer {
    String processor;
    int memory;

    public Computer(String processor, int memory) {
        this.processor = processor;
        this.memory = memory;
    }

    void printInfo() {
        System.out.println(processor + " " + memory);
    }
}
