package com.company;

import java.util.Random;
import java.util.Scanner;

public class GuessGame {
    public static void main(String[] args) {
        Scanner reader = new Scanner(System.in);

        Random random = new Random();
        int number =random.nextInt(10000);
        int userInput;
        int counter = 1;

        System.out.println("Zgadnij liczbę:");

        while ((userInput = reader.nextInt()) != number) {
            if (userInput > number) {
                System.out.println("Podana liczba jest za duża, zgaduj dalej!");
                counter++;
            } else {
                System.out.println("Podana liczba jest za mała, zgadu dalej!");
                counter++;
            }
        }
        System.out.println("Brawo, zgadłeś o jaką liczbę chodziło! Potrzebowałeś do tego " + counter + " prób.");
        reader.close();
    }
}
