package com.company.twoDimensionalArray;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        double[][] tab = {{1.0, 1.5, 2.0}, {1.5, 2.0, 2.5}, {2.0, 2.5, 3.0}};
        System.out.println(sumOfProductsDiagonl(tab));
        System.out.println(sumOfProductsCentreLineAndCentreColumn(tab));
        System.out.println(sumOfAllEdgesElements(tab));
    }

    static double sumOfProductsDiagonl(double[][] tab) {
        return (tab[0][0] * tab[1][1] * tab[2][2] + tab[0][2] * tab[1][1] * tab[2][0]);
    }

    static double sumOfProductsCentreLineAndCentreColumn(double[][] tab) {
        double sumCentreLine = Arrays.stream(tab[1]).sum();
        double sumCentreColumn = tab[0][1] + tab[1][1] + tab[2][1];

        return sumCentreColumn * sumCentreLine;
    }

    static double sumOfAllEdgesElements(double[][] tab) {
        double sumOfAllEmelemnts = Arrays.stream(tab).flatMapToDouble(Arrays::stream).sum();
        return sumOfAllEmelemnts - tab[1][1];
    }
}
