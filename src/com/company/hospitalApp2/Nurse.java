package com.company.hospitalApp2;

public class Nurse extends Person {
    private int overtime;

    public Nurse(String name, String surname, int payment, int overtime) {
        super(name, surname, payment);
        this.overtime = overtime;
    }

    public int getOvertime() {
        return overtime;
    }

    public void setOvertime(int overtime) {
        this.overtime = overtime;
    }

    @Override
    public String toString() {
        return super.toString() + ",Nadgodziny: " + overtime;
    }
}
