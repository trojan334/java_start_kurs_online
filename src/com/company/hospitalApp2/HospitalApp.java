package com.company.hospitalApp2;

public class HospitalApp {
    public static void main(String[] args) {
        Hospital hospital = new Hospital();
        hospital.add(new Doctor("Łukasz", "Trojanowicz", 15000, 2500));
        hospital.add(new Nurse("Kasia", "Nowak", 152000, 6));
        hospital.add(new Nurse("Anna", "Kowalska", 25000, 8));

        System.out.println("Pracownicy szpitala: ");
        System.out.println(hospital);
    }
}
