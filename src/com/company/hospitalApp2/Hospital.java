package com.company.hospitalApp2;

import java.util.Arrays;

public class Hospital {
    private static final int MAX_EMPLOYES = 3;

    private Person[] employees = new Person[MAX_EMPLOYES];
    private int employeesNumbr;

    public Person[] getEmployees() {
        return employees;
    }

    public void setEmployees(Person[] employees) {
        this.employees = employees;
    }

    public int getEmployeesNumbr() {
        return employeesNumbr;
    }

    public void setEmployeesNumbr(int employeesNumbr) {
        this.employeesNumbr = employeesNumbr;
    }

    public void add(Person person) {
        if (getEmployeesNumbr() < MAX_EMPLOYES) {
            getEmployees()[getEmployeesNumbr()] = person;
            setEmployeesNumbr(getEmployeesNumbr() + 1);
        }
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < employeesNumbr; i++) {
            result.append(employees[i]).append("\n");
        }
        return result.toString();
    }
}
