package com.company.hospitalApp2;

public class Person {
    private String name;
    private String surname;
    private int payment;

    public Person(String name, String surname, int payment) {
        this.name = name;
        this.surname = surname;
        this.payment = payment;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getPayment() {
        return payment;
    }

    public void setPayment(int payment) {
        this.payment = payment;
    }

    @Override
    public String toString() {
        return "Imię " + name + ",Nazwisko " + surname + ",Wypłata: " + payment;
    }
}
